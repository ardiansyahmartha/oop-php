Soal 1 Membuat Database

create database myshop;

Soal 2 Membuat Table di Dalam Database

 create table users(
    -> id int auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key (id)
    -> );

 create table categories(
    -> id int auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );

 create table items(
    -> id int auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> primary key(id),
    -> foreign key (category_id) references categories(id)
    -> );


Soal 3 Memasukkan Data pada Table

insert into users (name, email, password) values
    -> ("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");

insert into categories(name) values
    -> ("gadget"),("cloth"),("men"),("women"),("branded");

insert into items(name, description, price, stock, category_id) values
    -> ("Sumsang b50","hape keren dari merek sumsang","4000000","100", 1),("Uniklooh","baju keren dari brand ternama","500000","50", 2),("IMHO Watch","Jam tangan anak yang jujur banget","2000000","10", 1);

Soal 4 Mengambil Data dari Database

A.
select id, name, email from users;

B.
select * from items where price > "1000000";
select * from items where name like "%sang%";

C.
select items.id, items.name, items.description, items.price, items.stock, items.categories_id, categories.name from items inner join categories items.categories_id = categories.id;

Soal 5 Mengubah Data dari Database

update items set name = "sumsang", price ="2500000"
    -> where id =1;