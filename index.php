<?php
require_once ('animal.php');
require_once ('ape.php');
require_once ('frog.php');

$sheep = new animal("shaun");

echo "Nama Sheep: " .$sheep->name. "<br>"; // "shaun"
echo "Jumlah Kaki: " .$sheep->legs. "<br>"; // 4
echo "Cold Blooded: " .$sheep->cold_blooded. "<br> <br>";


$sungokong = new ape("kera sakti");

echo " Nama Ape: " .$sungokong->name. "<br>";
echo " Jumlah Kaki: " .$sungokong->legs. "<br>";
echo " Cold Blooded: " .$sungokong->cold_blooded. "<br>";
$sungokong->yell(). "<br><br>";

$kodok = new frog("buduk");

echo " Nama Kodok: " .$kodok->name. "<br>";
echo " Jumlah Kaki: " .$kodok->legs. "<br>";
echo " Cold Blooded: " .$kodok->cold_blooded. "<br>";
$kodok->jump(). "<br><br>";

